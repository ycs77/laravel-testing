<?php

namespace Tests\Feature;

use App\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    private $product;

    public function setUp()
    {
        parent::setUp();

        $this->product = factory(Product::class)->create();
    }

    public function testUserCanVisitAProductPageAndSeeProduct()
    {
        $response = $this->get(route('product.index'));
        $response->assertOk();
        $response->assertSee($this->product->name);
    }

    public function testAUserCanVisitASingleProductPage()
    {
        $response = $this->get(route('product.show', $this->product));
        $response->assertOk();
        $response->assertSee($this->product->name);
    }

    public function testLoginUserCanBuyProduct()
    {
        $this->authenticate();

        $data = [
            'product_id' => $this->product->id,
            'price' => 200,
        ];

        $response = $this->post(route('product.buy'), $data);
        $this->assertDatabaseHas('purchases', $data);
        $response->assertOk();
    }
}
