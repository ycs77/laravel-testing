<?php

namespace Tests\Unit;

use App\Exceptions\MinorCannotBuyAlcoholicProductException;
use App\Product;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductTest extends TestCase
{
    private $product;

    protected function setUp()
    {
        parent::setUp();

        $this->product = factory(Product::class)->make();
    }

    /**
     * @return void
     */
    public function testProductHasName()
    {
        $this->assertNotEmpty($this->product->name);
    }

    /**
     * @return void
     */
    public function testProductHasType()
    {
        $this->assertNotEmpty($this->product->type);
    }

    public function testMinorUserCannotButAlcoholicProduct()
    {
        $product = factory(Product::class)->make([
            'type' => 'Alcoholic',
        ]);

        $user = factory(User::class)->make([
            'age' => 16,
        ]);

        $this->actingAs($user); // Login

        $this->expectException(MinorCannotBuyAlcoholicProductException::class);

        $product->buy();
    }
}
