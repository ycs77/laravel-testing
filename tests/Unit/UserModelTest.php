<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserModelTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserHasFullnameAttribute()
    {
        $user = User::make([
            'name' => 'TestUser',
            'age' => 16,
            'email' => 'test@email.com',
            'password' => bcrypt('script'),
        ]);

        $this->assertNotEmpty($user->name);
    }

    public function testUserHasAgeAttribute()
    {
        $user = factory(User::class)->make();

        $this->assertNotEmpty($user->age);
    }
}
