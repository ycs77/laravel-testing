<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class VueTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExampleComponent()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertVueContains('details', 'John', '@example');
        });
    }
}
