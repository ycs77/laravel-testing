<?php

namespace Tests\Browser;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class WaitingTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(factory(User::class)->create())
                    ->visit('/home')
                    ->assertPathIs('/home')
                    ->click('#launchBtn')
                    ->whenAvailable('.modal', function ($modal) {
                        $modal->assertSee('Modal title')
                              ->click('#close');
                    })
                    ->assertSee('Johnny.');
        });
    }
}
