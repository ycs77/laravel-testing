<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RegisterTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * A Dusk test user can register.
     *
     * @return void
     */
    public function testUserCanRegister()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/register')
                    ->assertSee(__('Register'))
                    ->type('name', 'Johnny')
                    ->type('email', 'testing@email.com')
                    ->type('password', 'secret')
                    ->type('password_confirmation', 'secret')
                    ->press(__('Register'))
                    ->assertPathIs('/home');
        });
    }
}
