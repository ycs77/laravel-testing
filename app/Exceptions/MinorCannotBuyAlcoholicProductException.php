<?php

namespace App\Exceptions;

use Exception;

class MinorCannotBuyAlcoholicProductException extends Exception
{
    //
}
