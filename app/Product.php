<?php

namespace App;

use App\Exceptions\MinorCannotBuyAlcoholicProductException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Product extends Model
{
    public function buy()
    {
        if (Auth::user()->isMinor()){
            throw new MinorCannotBuyAlcoholicProductException;
        }

        return true;
    }
}
