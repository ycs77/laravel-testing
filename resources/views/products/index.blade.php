@extends('layouts.app')

@section('content')

    @foreach ($products as $product)
        <h1>
            <a href="{{ route('product.show', $product) }}">
                {{ $product->name }}
            </a>
        </h1>
    @endforeach

@endsection
